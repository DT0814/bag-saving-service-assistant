package com.twuc.bagSaving.assistant;

import com.twuc.bagSaving.Bag;
import com.twuc.bagSaving.Ticket;

public interface Assistant {
    Ticket save(Bag bag);


    Bag getBag(Ticket ticket);
}
