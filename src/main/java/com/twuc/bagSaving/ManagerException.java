package com.twuc.bagSaving;

public class ManagerException extends RuntimeException{
    public ManagerException(String message) {
        super(message);
    }
}
