package com.twuc.bagSaving;

public class AssistantSaveException extends RuntimeException {
    public AssistantSaveException(String message) {
        super(message);
    }
}
