package com.twuc.bagSaving.assistant;

import com.twuc.bagSaving.*;

import java.util.Arrays;
import java.util.List;

public class LazyAssistant implements Assistant {
    private List<Cabinet> cabinets;

    public LazyAssistant(Cabinet... cabinet) {

        this.cabinets = Arrays.asList(cabinet);
    }

    @Override
    public Ticket save(Bag bag) {
        if (null == cabinets) {
            throw new IllegalArgumentException("cabinets is empty");
        }
        LockerSize lockerSize = Utils.getLockerSize(bag);
        Ticket ticket = null;
        if (null == lockerSize) {
            throw new AssistantSaveException("error");
        }
        ticket = Utils.getTicketOnCabinetOrder(bag, lockerSize, cabinets);
        if (null == ticket) {
            throw new AssistantSaveException("error");
        }
        return ticket;
    }


    @Override
    public Bag getBag(Ticket ticket) {
        for (Cabinet cabinet : cabinets) {
            try {
                Bag bag = cabinet.getBag(ticket);
                if (bag != null) {
                    return bag;
                }
            }catch (IllegalArgumentException ex){

            }
        }
        return null;
    }
}
