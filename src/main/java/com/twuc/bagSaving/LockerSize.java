package com.twuc.bagSaving;

public enum LockerSize {
    BIG(40),
    MEDIUM(30),
    SMALL(20);

    private final int sizeNumber;

    LockerSize(int sizeNumber) {
        this.sizeNumber = sizeNumber;
    }

    public int getSizeNumber() {
        return sizeNumber;
    }
}
