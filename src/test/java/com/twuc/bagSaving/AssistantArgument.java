package com.twuc.bagSaving;

import com.twuc.bagSaving.assistant.LazyAssistant;
import com.twuc.bagSaving.assistant.MasterAssistant;
import com.twuc.bagSaving.assistant.StupidAssistant;

public class AssistantArgument {
    public static Object[][] createAssistant() {
        return new Object[][]{
                new Object[]{
                        new LazyAssistant(new Cabinet(LockerSetting.of(LockerSize.SMALL, 1)), new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1))),
                        new StupidAssistant(new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1)), new Cabinet(LockerSetting.of(LockerSize.BIG, 1))),
                        BagSize.MEDIUM},
                new Object[]{
                        new StupidAssistant(new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1)), new Cabinet(LockerSetting.of(LockerSize.BIG, 1))),
                        new MasterAssistant(new Cabinet(LockerSetting.of(LockerSize.BIG, 1)), new Cabinet(LockerSetting.of(LockerSize.SMALL, 1))),
                        BagSize.BIG},
                new Object[]{
                        new MasterAssistant(new Cabinet(LockerSetting.of(LockerSize.BIG, 1)), new Cabinet(LockerSetting.of(LockerSize.SMALL, 1))),
                        new LazyAssistant(new Cabinet(LockerSetting.of(LockerSize.SMALL, 1)), new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1))),
                        BagSize.SMALL},
        };
    }
    public static Object[][] createBadAssistant() {
        return new Object[][]{
                new Object[]{
                        new LazyAssistant(new Cabinet(LockerSetting.of(LockerSize.SMALL, 1)), new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1))),
                        new StupidAssistant(new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1)), new Cabinet(LockerSetting.of(LockerSize.SMALL, 1))),
                        BagSize.BIG},
                new Object[]{
                        new StupidAssistant(new Cabinet(LockerSetting.of(LockerSize.SMALL, 1)), new Cabinet(LockerSetting.of(LockerSize.SMALL, 1))),
                        new MasterAssistant(new Cabinet(LockerSetting.of(LockerSize.SMALL, 1)), new Cabinet(LockerSetting.of(LockerSize.SMALL, 1))),
                        BagSize.MEDIUM},
                new Object[]{
                        new MasterAssistant(new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1)), new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1))),
                        new LazyAssistant(new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1)), new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1))),
                        BagSize.BIG},
        };
    }
}
