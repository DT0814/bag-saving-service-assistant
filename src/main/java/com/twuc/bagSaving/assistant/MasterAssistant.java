package com.twuc.bagSaving.assistant;

import com.twuc.bagSaving.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author tao.dong
 */
public class MasterAssistant implements Assistant {
    private List<Cabinet> cabinets;

    public MasterAssistant(Cabinet... cabinet) {

        this.cabinets = Arrays.asList(cabinet);
    }

    @Override
    public Ticket save(Bag bag) {
        if (null == cabinets) {
            throw new IllegalArgumentException("cabinets is empty");
        }
        LockerSize lockerSize = null;
        for (LockerSize value : LockerSize.values()) {
            if (bag.getBagSize().getSizeNumber() == value.getSizeNumber()) {
                lockerSize = value;
            }
        }
        Ticket ticket = Utils.getTicketByBagSizeEqualsLockerSize(bag, lockerSize, cabinets);
        if (null != ticket) {
            return ticket;
        }
        ticket = Utils.getTicketOnCabinetOrder(bag, lockerSize, cabinets);
        if (null == ticket) {
            throw new AssistantSaveException("error");
        }
        return ticket;
    }

    @Override
    public Bag getBag(Ticket ticket) {
        for (Cabinet cabinet : cabinets) {
            try {
                Bag bag = cabinet.getBag(ticket);
                if (bag != null) {
                    return bag;
                }
            }catch (IllegalArgumentException ex){

            }
        }
        return null;
    }
}
