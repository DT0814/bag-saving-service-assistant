package com.twuc.bagSaving;

import com.twuc.bagSaving.assistant.Assistant;
import com.twuc.bagSaving.assistant.LazyAssistant;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LazyAssistantTest extends BagSavingArgument {
    Assistant assistant;

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createCabinetByLazy"})
    void should_be_return_ticket_when_give_two_cabinet_by_lazy(Cabinet cabinet, Cabinet cabinet1, BagSize bagSize) {
        //Arrange
        assistant = new LazyAssistant(cabinet, cabinet1);
        Bag bag = new Bag(bagSize);
        //Act
        Ticket ticket = assistant.save(bag);
        //Assert
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createCabinetByLazyBad"})
    void should_be_return_ticket_when_give_bad_data(Cabinet cabinet, Cabinet cabinet1, BagSize bagSize) {
        //Arrange
        assistant = new LazyAssistant(cabinet, cabinet1);
        Bag bag = new Bag(bagSize);
        //Act
        //Assert
        assertThrows(AssistantSaveException.class, () -> assistant.save(bag));
    }
}
