package com.twuc.bagSaving;

import com.twuc.bagSaving.assistant.Assistant;
import com.twuc.bagSaving.assistant.StupidAssistant;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

public class StupidAssistantTest extends BagSavingArgument {
    Assistant stupidAssistant;

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBag"})
    void should_be_return_ticket_when_give_package(Cabinet cabinet, BagSize bagSize) {
        //Arrange
        stupidAssistant = new StupidAssistant(cabinet);
        //Act
        Ticket ticket = stupidAssistant.save(new Bag(bagSize));
        //Assert
        assertNotNull(ticket);

    }


    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBag"})
    void should_be_return_bag_when_give_ticket_and_equals_pre_bag(Cabinet cabinet, BagSize bagSize) {
        //Arrange
        stupidAssistant = new StupidAssistant(cabinet);
        Bag bag = new Bag(bagSize);
        Ticket ticket = stupidAssistant.save(bag);
        //Act
        Bag resBag = stupidAssistant.getBag(ticket);
        //Assert
        assertSame(bag, resBag);

    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createNotHaveSizeBag"})
    void should_be_return_bag_when_give_not_have_locker_size_bag_throw_err(Cabinet cabinet, BagSize bagSize) {
        //Arrange
        stupidAssistant = new StupidAssistant(cabinet);
        Bag bag = new Bag(bagSize);
        //Act
        assertThrows(AssistantSaveException.class, () -> stupidAssistant.save(bag));
        //Assert
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createCabinet"})
    void should_be_return_ticket_when_give_two_cabinet(Cabinet cabinet, Cabinet cabinet1, BagSize bagSize) {
        //Arrange
        stupidAssistant = new StupidAssistant(cabinet, cabinet1);
        Bag bag = new Bag(bagSize);
        //Act
        Ticket ticket = stupidAssistant.save(bag);
        //Assert
        assertNotNull(ticket);
    }

}
