package com.twuc.bagSaving.assistant;

import com.twuc.bagSaving.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author tao.dong
 */
public class StupidAssistant implements Assistant {
    private List<Cabinet> cabinets;

    public StupidAssistant(Cabinet... cabinet) {
        this.cabinets = Arrays.asList(cabinet);
    }

    @Override
    public Ticket save(Bag bag) {
        LockerSize lockerSize = Utils.getLockerSize(bag);
        Ticket ticket = Utils.getTicketByBagSizeEqualsLockerSize(bag, lockerSize, cabinets);
        if (null == ticket) {
            throw new AssistantSaveException("error");
        }
        return ticket;
    }


    @Override
    public Bag getBag(Ticket ticket) {
        for (Cabinet cabinet : cabinets) {
            try {
                Bag bag = cabinet.getBag(ticket);
                if (bag != null) {
                    return bag;
                }
            }catch (IllegalArgumentException ex){

            }
        }
        return null;
    }
}
