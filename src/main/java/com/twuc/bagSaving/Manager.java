package com.twuc.bagSaving;

import com.twuc.bagSaving.assistant.Assistant;
import sun.text.CollatorUtilities;

import java.util.Arrays;
import java.util.List;

public class Manager {
    private List<Assistant> assistants;

    public Manager(Assistant... assistants) {
        this.assistants = Arrays.asList(assistants);
    }

    public Ticket save(Bag bag) {
        if (null == assistants || assistants.size() == 0) {
            throw new ManagerException("assistants is empty");
        }
        for (Assistant assistant : assistants) {
            Ticket ticket = assistant.save(bag);
            if (null != ticket) {
                return ticket;
            }
        }
        throw new ManagerException("not support bag size:" + bag.getBagSize());
    }


    public Bag getBag(Ticket ticket) {
        if (null == ticket) {
            throw new ManagerException("null ticket");
        }
        for (Assistant assistant : assistants) {
            try {
                Bag bag = assistant.getBag(ticket);
                if (null != bag) {
                    return bag;
                }
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
            }
        }
        throw new ManagerException("invalid ticket");
    }
}
