package com.twuc.bagSaving.assistant;

import com.twuc.bagSaving.*;

import java.util.List;

public class Utils {
    public static LockerSize lockerSizeAdd(LockerSize lockerSize) {
        if (lockerSize == LockerSize.SMALL) {
            return LockerSize.MEDIUM;
        }
        if (lockerSize == LockerSize.MEDIUM) {
            return LockerSize.BIG;
        }
        return null;
    }

    public static LockerSize getLockerSize(Bag bag) {
        LockerSize lockerSize = null;
        for (LockerSize value : LockerSize.values()) {
            if (bag.getBagSize().getSizeNumber() == value.getSizeNumber()) {
                lockerSize = value;
            }
        }
        return lockerSize;
    }

    public static Ticket getTicketByBagSizeEqualsLockerSize(Bag bag, LockerSize lockerSize, List<Cabinet> cabinets) {
        Ticket ticket = null;
        for (Cabinet cabinet : cabinets) {
            try {
                ticket = cabinet.save(bag, lockerSize);
            } catch (RuntimeException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return ticket;
    }

    public static Ticket getTicketOnCabinetOrder(Bag bag, LockerSize lockerSize, List<Cabinet> cabinets) {
        Ticket ticket = null;
        for (Cabinet cabinet : cabinets) {
            LockerSize newLockerSize = lockerSize;
            while (newLockerSize != null) {
                try {
                    ticket = cabinet.save(bag, newLockerSize);
                } catch (IllegalArgumentException | InsufficientLockersException ex) {
                    if (("Not supported locker size: " + lockerSize).equals(ex.getMessage())
                            || "Insufficient empty lockers.".equals(ex.getMessage())) {
                        newLockerSize = Utils.lockerSizeAdd(newLockerSize);
                    } else {
                        throw ex;
                    }
                }
            }
        }
        return ticket;
    }

}
