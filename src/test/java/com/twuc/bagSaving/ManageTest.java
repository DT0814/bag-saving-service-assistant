package com.twuc.bagSaving;

import com.twuc.bagSaving.assistant.Assistant;
import com.twuc.bagSaving.assistant.LazyAssistant;
import com.twuc.bagSaving.assistant.MasterAssistant;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

public class ManageTest extends AssistantArgument {
    Manager manager;

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.AssistantArgument#createAssistant"})
    void should_be_return_ticket_when_give_two_assistant(Assistant assistant, Assistant assistant1, BagSize bagSize) {
        //Arrange
        manager = new Manager(assistant, assistant1);
        Bag bag = new Bag(bagSize);
        //Act
        Ticket ticket = manager.save(bag);
        //Assert
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.AssistantArgument#createAssistant"})
    void should_be_return_bag_when_give_ticket_assistant(Assistant assistant, Assistant assistant1, BagSize bagSize) {
        //Arrange
        manager = new Manager(assistant, assistant1);
        Bag bag = new Bag(bagSize);
        Ticket ticket = manager.save(bag);
        //Act
        Bag res = manager.getBag(ticket);
        //Assert
        assertSame(bag, res);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.AssistantArgument#createBadAssistant"})
    void should_be_return_when_save_throw_error(Assistant assistant, Assistant assistant1, BagSize bagSize) {
        //Arrange
        manager = new Manager(assistant, assistant1);
        Bag bag = new Bag(bagSize);
        //Act
        //Assert
        assertThrows(AssistantSaveException.class, () -> manager.save(bag));
    }
}
